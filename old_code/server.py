"""
Simple demonstration of how to implement Server-sent events (SSE) in Python
using Bottle micro web-framework.
SSE require asynchronous request handling, but it's tricky with WSGI. One way
to achieve that is to use gevent library as shown here.
Usage: just start the script and open http://localhost:8080/ in your browser.
Based on:
- "Primer to Asynchronous Applications",
	http://bottlepy.org/docs/dev/async.html
- "Using server-sent events",
	https://developer.mozilla.org/en-US/docs/Server-sent_events/Using_server-sent_events
"""
import sys
import time
import numpy as np

# Bottle requires gevent.monkey.patch_all() even if you don't like it.
from gevent import monkey; monkey.patch_all()
from gevent import sleep

from bottle import get, post, request, response, static_file
from bottle import GeventServer, run


fid = open('wgtn_outline.csv','rb')
fid.readline() # header
l = fid.readline() # outline data
# parse this
a = l.split('(')
wgtn_outline = []
for i in [3,5,7,9,11,13,15,17,19,21,22]:
    polygon = []
    for aa in a[i].split(')')[0].split(','):
        polygon.append( [float(aaa) for aaa in aa.split(' ')] )
    wgtn_outline.append( np.array(polygon) )


sse_test_page1 = """
<html>
<head>
<style> 
.flex-container {
    display: flex;
    width: 1000px;
    height: 700px;
    background-color: lightgrey;
}
.flex-item {
    background-color: silver;
    margin: 10px;
}
.item1 {
    flex: 4;
}
.item2 {
    flex: 6;
}
fieldset {
    width:300px;
}
legend {
    font-size: 14px;
    font-weight: bold;
}
label.field {
    text-align:right;
    float:left;
    width:100px;
    font-size: 12px;
}
input.myslider {
    float:right;
    width: 170px;
}
fieldset p {
    clear:both;
    padding:5px;
}
</style>
<script src="svg-pan-zoom.min.js"></script>
</head>
<body>
<div class="flex-container">
  <div class="flex-item item1">
    <p id="thedump"></p>
"""

def make_form():
    global contact_R, contact_prob, heal_prob, die_prob, immu_prob, infe_prob
    global TIMESTEPS_TO_INFECTIOUS, TIMESTEPS_TO_SYMPTOMS, TIMESTEPS_TO_VACCINATION
    
    print(immu_prob*100, infe_prob*100, contact_prob*100, heal_prob*1000, die_prob*1000, TIMESTEPS_TO_INFECTIOUS, TIMESTEPS_TO_SYMPTOMS, TIMESTEPS_TO_VACCINATION)

    form  = '<form action="/restart" method="post">\n'
    form += '<fieldset>\n'
    form += '<legend>Population & disease specs</legend>\n'
    form += '      <p><label class="field" for="Vaccinated" id="immu_prob_label">Vaccinated</label>\n'
    form += '        <input class="myslider" id="immu_prob" name="immu_prob" type="range" min="0" max="100" value="%d" step="5" onChange="formwaschanged()" />\n' % (immu_prob*100)
    form += '      </p>\n'
    form += '      <p><label class="field" for="Infected" id="infe_prob_label">Infected</label>\n'
    form += '        <input class="myslider" id="infe_prob" name="infe_prob" type="range" min="0" max="1000" value="%d" step="1" onChange="formwaschanged()"/>\n' % (infe_prob*1000)
    form += '      </p>\n'
    form += '      <p><label class="field" for="Transmit" id="transmit">Chance to transmit</label>\n'
    form += '        <input class="myslider" id="contact_prob" name="contact_prob" type="range" min="0" max="100" value="%d" step="5" onChange="formwaschanged()" />\n' % (contact_prob*100)
    form += '      </p>\n'
    form += '      <p><label class="field" for="Heal" id="heal">Chance to heal</label>\n'
    form += '        <input class="myslider" id="chance_heal" name="chance_heal" type="range" min="0" max="250" value="%d" step="1" onChange="formwaschanged()" />\n' % (heal_prob*1000)
    form += '      </p>\n'
    form += '      <p><label class="field" for="Mortality" id="mortality">Mortality:</label>\n'
    form += '        <input class="myslider" id="chance_die" name="chance_die" type="range" min="0" max="250" value="%d" step="1" onChange="formwaschanged()" />\n' % (die_prob*1000)
    form += '      </p>\n'
    form += '      <p><label class="field" for="TSTINF" id="TSTINF">Infectious after</label>\n'
    form += '        <input class="myslider" id="timesteps_to_infectious" name="timesteps_to_infectious" type="range" min="0" max="100" value="%d" step="1" onChange="formwaschanged()" />\n' % (TIMESTEPS_TO_INFECTIOUS)
    form += '      </p>\n'
    form += '      <p><label class="field" for="TSTSYM" id="TSTSYM">Symptomatic after</label>\n'
    form += '        <input class="myslider" id="timesteps_to_symptomatic" name="timesteps_to_symptomatic" type="range" min="0" max="200" value="%d" step="1" onChange="formwaschanged()" />\n' % (TIMESTEPS_TO_SYMPTOMS)
    form += '      </p>\n'
    form += '      <p><label class="field" for="TSTTRE" id="TSTTRE">Treatment after</label>\n'
    form += '        <input class="myslider" id="timesteps_to_vaccination" name="timesteps_to_vaccination" type="range" min="0" max="400" value="%d" step="1" onChange="formwaschanged()" />\n' % (TIMESTEPS_TO_VACCINATION)
    form += '      </p>\n'
    form += '     <br>\n'
    form += '     <button type="button" onclick="resetformvalues()" style="float:left; font-size:10px">reset to defaults</button>\n'
    form += '     <input value="Restart simulation" type="submit" style="float:right">\n'
    form += '    </form>\n'
    return form

sse_test_page2 = """
    <br><br>
<p id='sum_alive' style="align:right;font-size:12px">N_alive: </p>
<p id='sum_immune' style="align:right;font-size:12px">N_immune: </p>
<p id='sum_infected' style="align:right;font-size:12px">N_infected: </p>
<p id='sum_infectious' style="align:right;font-size:12px">N_infectious: </p>
<p id='sum_symptomatic' style="align:right;font-size:12px">N_symptomatic: </p>
<p id='sum_dead' style="align:right;font-size:12px">N_dead: </p>
  </div>
  <div class="flex-item item2">
    <p id="timestepcounter">Time elapsed: 0 days</p>
"""
#<canvas id="tada" width="1024" height="600">
#</canvas>

sse_test_page3 = """
  </div>
</div>
<script>
function resetformvalues() {
   document.getElementById("immu_prob").value = 95;
   document.getElementById("infe_prob").value = 1;
   document.getElementById("contact_prob").value = 50;
   document.getElementById("chance_heal").value = 10;
   document.getElementById("chance_die").value = 1;
   document.getElementById("timesteps_to_infectious").value = 10;
   document.getElementById("timesteps_to_symptomatic").value = 14;
   document.getElementById("timesteps_to_vaccination").value = 100;
   formwaschanged();
};
function formwaschanged() {
   document.getElementById("immu_prob_label").innerHTML = "Vaccinated ("+document.getElementById("immu_prob").value.toString()+"%)";
   document.getElementById("infe_prob_label").innerHTML = "Infected ("+(document.getElementById("infe_prob").value/10).toString()+"%)";
   document.getElementById("transmit").innerHTML = "P_transmit ("+document.getElementById("contact_prob").value.toString()+"%)";
   document.getElementById("heal").innerHTML = "P_heal ("+(document.getElementById("chance_heal").value/10).toString()+"%)";
   document.getElementById("mortality").innerHTML = "P_die ("+(document.getElementById("chance_die").value/10).toString()+"%)";
   document.getElementById("TSTINF").innerHTML = "Infectious after ("+(document.getElementById("timesteps_to_infectious").value/4).toString()+"d)";
   document.getElementById("TSTSYM").innerHTML = "Symptomatic after ("+(document.getElementById("timesteps_to_symptomatic").value/4).toString()+"d)";
   document.getElementById("TSTTRE").innerHTML = "Treatment after ("+(document.getElementById("timesteps_to_vaccination").value/4).toString()+"d)";
};
formwaschanged();

var panZoomTiger = svgPanZoom('#themap', {zoomEnabled:true, controlIconsEnabled:true});

function draw(myPoly,myArray) {
   var palive = document.getElementById(myPoly);
   palive.setAttribute("points", myArray)
};

function start_stream() {
   var es = new EventSource("stream");
   es.onmessage = function (e) {
   var myArray = e.data.split('---');
   // first myArray element contains time step
   document.getElementById("timestepcounter").innerHTML = "Time elapsed: "+(Math.floor(myArray[0]/4.0)).toString()+" days"
   // next six contain last histogram values
   document.getElementById("sum_alive").innerHTML = "N_alive: "+myArray[1]
   document.getElementById("sum_immune").innerHTML = "N_immune: "+myArray[2]
   document.getElementById("sum_infected").innerHTML = "N_infected: "+myArray[3]
   document.getElementById("sum_infectious").innerHTML = "N_infectious: "+myArray[4]
   document.getElementById("sum_symptomatic").innerHTML = "N_symptomatic: "+myArray[5]
   document.getElementById("sum_dead").innerHTML = "N_dead: "+myArray[6]
   // next six contain histogram polygons
   draw('phalive',myArray[7]);
   draw('phimmune',myArray[8]);
   draw('phinfected',myArray[9]);
   draw('phinfectious',myArray[10]);
   draw('phsymptomatic',myArray[11]);
   draw('phdead',myArray[12]);
   // all others contain polygons (their vertices)
   draw('palive',myArray[13]);
   draw('pimmune',myArray[14]);
   draw('pinfected',myArray[15]);
   draw('pinfectious',myArray[16]);
   draw('psymptomatic',myArray[17]);
   draw('pdead',myArray[18]);
//   window.requestAnimationFrame(draw);
  };
};
start_stream();
</script>

</body>
</html>
"""

def make_chart_svg():
    xmax = 90
    svg  = '<svg id="thechart" height="100" width="280" draggable="true" style="background-color:white;border:1px solid black;float:right;margin:10px" preserveAspectRatio="none">\n'
    svg += '<polygon id="phalive" point="0,0" style="fill:green"/>'
    svg += '<polygon id="phimmune" point="0,0" style="fill:lightgreen"/>'
    svg += '<polygon id="phinfected" point="0,0" style="fill:yellow"/>'
    svg += '<polygon id="phinfectious" point="0,0" style="fill:orange"/>'
    svg += '<polygon id="phsymptomatic" point="0,0" style="fill:red"/>'
    svg += '<polygon id="phdead" point="0,0" style="fill:black"/>'
    # # axis
    # axisstylex = "stroke:black;stroke-width:%f" % (.1/90)
    # axisstyley = "stroke:black;stroke-width:%f" % (.1)
    # svg += '<line x1="0" x2="%d" y1="0" y2="0" style="%s"></line>\n' % (xmax,axisstylex)
    # svg += '<line x1="0" x2="0" y1="0" y2="1" style="%s"></line>\n' % (axisstyley)
    # # grid lines
    # xgridstep = 5
    # gridstylex = "stroke:grey;stroke-width:%f" % (.1/90)
    # gridstyley = "stroke:grey;stroke-width:%f" % (.1)
    # svg += '<g id="xGrid" style="%s">\n' % (gridstylex)
    # for x in np.arange(0,xmax,xgridstep):
    #     svg += '<line x1="%d" x2="%d" y1="0" y2="1"></line>\n' % (x,x)
    # svg += '</g>\n'
    # svg += '<g id="yGrid" style="%s">\n' % (gridstyley)
    # for y in [0,.2,.4,.6,.8,1.]:
    #     svg += '<line x1="0" x2="%d" y1="%d" y2="%d"></line>\n' % (xmax,y,y)
    # svg += '</g>\n'
    svg += '</svg>\n'
    return svg


poffsetx = 1741000
poffsety = 5450300

def make_map_svg():
    landcolor  = 'grey'
    watercolor = 'cornflowerblue'
    markerRadius = 4
    markerAlpha = 1
    def make_polygon(poly, style):
        scale = 50.
        polystr = '<polygon points="'
        for p in poly:
            pstr = '%d,%d ' % (p[0]-poffsetx,-(p[1]-poffsety))   #( (p[0]-poffsetx)/scale, (p[1]-poffsety)/scale )
            polystr += pstr
        polystr += '" style="%s"' % style
        polystr += '/>\n'
        return polystr

    svg  = '<svg id="themap" height="600" width="600" draggable="true" style="border:1px solid black;float:right;margin:10px" viewbox="%d,%d,%d,%d">\n' % (0,0,31000,31000)
    svg += '<g transform="scale(1,1)">\n'
    svg += '<polygon points="-20000,-20000 -20000,200000 100000,200000 100000,-20000" style="fill:%s;stroke:black;stroke-width:1" />\n' % (watercolor)
    for ip,poly in enumerate(wgtn_outline):
        if ip==10:    # pauatahanui inlet
            svg += make_polygon(poly,'fill:%s;stroke:black;stroke-width:20' % (watercolor))
        else:
            svg += make_polygon(poly,'fill:%s;stroke:black;stroke-width:20' % (landcolor))

    svg += '<marker id="markerCircleAlive"       markerWidth="17" markerHeight="17" refX="8" refY="8"><circle cx="8" cy="8" r="%d" style="stroke:none;" fill="green"      opacity="%f" /></marker>\n' % (markerRadius, markerAlpha)
    svg += '<marker id="markerCircleImmune"      markerWidth="17" markerHeight="17" refX="8" refY="8"><circle cx="8" cy="8" r="%d" style="stroke:none;" fill="lightgreen" opacity="%f" /></marker>\n' % (markerRadius, markerAlpha)
    svg += '<marker id="markerCircleInfected"    markerWidth="17" markerHeight="17" refX="8" refY="8"><circle cx="8" cy="8" r="%d" style="stroke:none;" fill="yellow"     opacity="%f" /></marker>\n' % (markerRadius, markerAlpha)
    svg += '<marker id="markerCircleInfectious"  markerWidth="17" markerHeight="17" refX="8" refY="8"><circle cx="8" cy="8" r="%d" style="stroke:none;" fill="orange"     opacity="%f" /></marker>\n' % (markerRadius, markerAlpha)
    svg += '<marker id="markerCircleSymptomatic" markerWidth="17" markerHeight="17" refX="8" refY="8"><circle cx="8" cy="8" r="%d" style="stroke:none;" fill="red"        opacity="%f" /></marker>\n' % (markerRadius, markerAlpha)
    svg += '<marker id="markerCircleDead"        markerWidth="17" markerHeight="17" refX="8" refY="8"><circle cx="8" cy="8" r="%d" style="stroke:none;" fill="black"      opacity="%f" /></marker>\n' % (markerRadius, markerAlpha)
    svg += '<polygon id="palive"       points="1000,1000 1000,2000 2000,2000 2000,1000"   style="fill:none;stroke:none;stroke-width:20;marker-mid:url(#markerCircleAlive);marker-start:url(#markerCircleAlive)" />\n'
    svg += '<polygon id="pimmune"      points="3000,1000 3000,2000 4000,2000 4000,1000"   style="fill:none;stroke:none;stroke-width:20;marker-mid:url(#markerCircleImmune);marker-start:url(#markerCircleImmune)" />\n'
    svg += '<polygon id="pinfected"    points="3000,1000 3000,2000 4000,2000 4000,1000"   style="fill:none;stroke:none;stroke-width:20;marker-mid:url(#markerCircleInfected);marker-start:url(#markerCircleInfected)" />\n'
    svg += '<polygon id="pinfectious"  points="5000,1000 5000,2000 6000,2000 6000,1000"   style="fill:none;stroke:none;stroke-width:20;marker-mid:url(#markerCircleInfectious);marker-start:url(#markerCircleInfectious)" />\n'
    svg += '<polygon id="psymptomatic" points="7000,1000 7000,2000 8000,2000 8000,1000"   style="fill:none;stroke:none;stroke-width:20;marker-mid:url(#markerCircleSymptomatic);marker-start:url(#markerCircleSymptomatic)" />\n'
    svg += '<polygon id="pdead"        points="9000,1000 9000,2000 10000,2000 10000,1000" style="fill:none;stroke:none;stroke-width:20;marker-mid:url(#markerCircleDead);marker-start:url(#markerCircleDead)" />\n'

    svg += '</svg>'
    return svg

def load_building_data():
    bldgs      = np.load('bldgs.npy')
    btype      = np.load('btype.npy')
    hpos       = np.load('hpos.npy')
    allresi    = np.load('allresi.npy')
    allother   = np.load('allother.npy')
    allschools = np.load('allschools.npy')
    allpubtrans = np.load('allpubtrans.npy')
    bldg_dict = {'bldgs':bldgs, 'btype':btype, 'hpos':hpos, 'allresi':allresi, 'allother':allother, 'allschools':allschools, 'allpubtrans':allpubtrans}
    return bldg_dict

def init_locations(bldg_dict):
    CHANCE_CHILD = .3
    CHANCE_RETIRED = .15
    CHANCE_PUBLIC_TRANSPORT = .2

    print("sampling people locations")
    sys.stdout.flush()
    
    btype       = bldg_dict['btype']
    hpos        = bldg_dict['hpos']
    allresi     = bldg_dict['allresi']
    allother    = bldg_dict['allother']
    allschools  = bldg_dict['allschools']
    allpubtrans = bldg_dict['allpubtrans']
    Nhouses = len(hpos)
    print(hpos.shape)

    occu_pdf = np.round( np.random.normal( loc=1., scale=1.2, size=(allresi.size,) ) )
    for ar in allresi:
        if btype[ar]=='apartments':
            occu_pdf[ar] = np.round( np.random.normal( loc=10., scale=12. ) )
    occu_pdf = np.clip( occu_pdf, 0, 100 )
    occu_pdf_bak = occu_pdf.copy()

    Npeople = int(sum(occu_pdf))
    plocs = np.zeros( (Npeople,5), dtype=np.int )
    print("Npeople: %d" % (Npeople))

    curbldg = 0
    curpers = 0
    while curpers < Npeople:
        if occu_pdf[curbldg]>0:
            plocs[curpers,0] = curbldg
            occu_pdf[curbldg] -= 1
            curpers += 1
        else:
            curbldg += 1

    for n in range(Npeople):
        plocs[n,1] = allother[ np.random.randint( low=0, high=allother.size ) ]
        plocs[n,2] = np.random.randint( low=0, high=Nhouses )
        plocs[n,3] = allother[ np.random.randint( low=0, high=allother.size ) ]
        plocs[n,4] = allresi[ np.random.randint( low=0, high=allresi.size ) ]

        if np.random.random()<CHANCE_CHILD:
            plocs[n,1] = allschools[ np.random.randint( low=0, high=allschools.size ) ]
        elif np.random.random()<CHANCE_RETIRED:
            plocs[n,2] = plocs[n,0]   # stays home a lot    

        if np.random.random()<CHANCE_PUBLIC_TRANSPORT:
            plocs[n,2] = allpubtrans[ np.random.randint( low=0, high=allpubtrans.size ) ]
        
    print("init_sample_locations done.")
    sys.stdout.flush()

    return Npeople,plocs

def init_bool_fields():
    global isalive,isimmune,isinfected,isinfectious,issymptomatic,isdead,diseaselength
    global halive,himmune,hinfected,hinfectious,hsymptomatic,hdead, Nhealed, Nvaccinated
    # indicator boolean fields
    isalive       = np.ones( (Npeople,), dtype=np.bool )
    isimmune      = np.zeros( (Npeople,), dtype=np.bool )  # init below
    isinfected    = np.zeros( (Npeople,), dtype=np.bool )  # init below
    isinfectious  = np.zeros( (Npeople,), dtype=np.bool )
    issymptomatic = np.zeros( (Npeople,), dtype=np.bool )
    isdead        = np.zeros( (Npeople,), dtype=np.bool )
    diseaselength = np.zeros( (Npeople,), dtype=np.int )
    # history lists for the boolean fields
    halive = []
    himmune= []
    hinfected = []
    hinfectious = []
    hsymptomatic = []
    hdead  = []
    # init immune
    isimmune = np.random.random(size=(Npeople,)) <= immu_prob
    # init infected
    isinfected = np.random.random(size=(Npeople,)) <= infe_prob
    Nhealed = 0
    Nvaccinated = 0

def reset_disease_props():
    global contact_R, contact_prob, heal_prob, die_prob, immu_prob, infe_prob
    global TIMESTEPS_TO_INFECTIOUS, TIMESTEPS_TO_SYMPTOMS, TIMESTEPS_TO_VACCINATION
    # contact radius and probability [m]
    contact_R = 10.
    contact_prob = .5
    # heal and die probabilites
    heal_prob = .01
    die_prob = .001
    # progression times
    TIMESTEPS_TO_INFECTIOUS = 10
    TIMESTEPS_TO_SYMPTOMS = 14
    TIMESTEPS_TO_VACCINATION = 100
    # immunity chance
    immu_prob = 0.95
    # infected chance
    infe_prob = 0.001

def update_stats():
    halive.append(       np.sum(isalive)       )
    hdead.append(        np.sum(isdead)        )
    himmune.append(      np.sum(isimmune)      )
    hinfected.append(    np.sum(isinfected)    )
    hinfectious.append(  np.sum(isinfectious)  )
    hsymptomatic.append( np.sum(issymptomatic) )

def print_stats(nt):
    if nt==0:
        print("timestep  \tNalive    \tNdead     \tNsick     \tNimmune   \tNinfected \tNinfectious\tNsymptomatic")
    print("%5d\t%10d\t%10d\t%10d\t%10d\t%10d\t%10d\t%10d\t%d\t%d" % (nt,\
                                                             halive[nt],\
                                                             hdead[nt],\
                                                             hinfected[nt]+hinfectious[nt]+hsymptomatic[nt],\
                                                             himmune[nt],\
                                                             hinfected[nt],\
                                                             hinfectious[nt],\
                                                            hsymptomatic[nt],\
                                                                     Nhealed,\
                                                                     Nvaccinated))

def get_new_locs( N, visit_pdf ):
    r = np.random.random( size=(N,) )
    for i,cvp in enumerate( np.cumsum(visit_pdf) ):
        r[r<cvp] = i+2
    r-=2
    return r.astype(np.int)

def init_bipartite_graph():
    global curlocs, dictlist

    curlocs = get_new_locs( Npeople, visit_pdf )
    ## construct dict of lists containing the people in each house
    dictlist = dict()
    # first init with empty list for each house
    Nhouses = len(bldg_dict['hpos'])
    for n in np.arange( Nhouses ):
        dictlist[n] = []
    # now populate with people in their current locations
    for i,p in enumerate(plocs):
        am_here = p[curlocs[i]]
        dictlist[ am_here ].append( i )

def move_people(Nmovespertimestep):
    global curlocs, dictlist, plocs
    # select random people
    rpeeps = np.random.randint( low=0, high=Npeople, size=(Nmovespertimestep,) )
    # get list of new location indices
    newlocs = get_new_locs( Nmovespertimestep, visit_pdf) 
    for i,r in enumerate(rpeeps):
        dictlist[ plocs[r,curlocs[r]] ].remove( r )  # remove from house
        dictlist[ plocs[r,newlocs[i]] ].append( r )  # add to new house
        curlocs[r] = newlocs[i]                      # record current location

def timestep():
    global Nhealed, Nvaccinated

    Nmovespertimestep = 1000
    move_people( Nmovespertimestep )
    # now find out who's sick
    sickly, = np.nonzero( np.logical_or( isinfectious, issymptomatic ) )
    issick  = reduce( np.logical_or, [isinfected,isinfectious,issymptomatic] )   # plain bool array
    # and for each of them 
    for s in sickly:
        # find out who's in the current location
        peeps = dictlist[ plocs[ s,curlocs[s] ] ]
        for p in peeps:
            if (p!=s) and isalive[p]:
                if not (issick[p] or isimmune[p]):
                    if np.random.random()<contact_prob:
                        isinfected[p] = True
    issick, = np.nonzero(issick)        # turn bool array into indices of sick people
    # advance disease time line
    diseaselength[issick] += 1    
    # perform progression
    for s in issick:
        # roll for healing chance
        if np.random.random() < heal_prob:   # healed!
            isimmune[s] = True
            isinfected[s] = False
            isinfectious[s] = False
            issymptomatic[s] = False            
            Nhealed += 1
        # time triggered progress
        if issymptomatic[s]:
            if np.random.random() < die_prob:       # died!
                isdead[s] = True
                isalive[s] = False
                isimmune[s] = False
            else:
                rtime = np.around( np.random.normal(loc=TIMESTEPS_TO_VACCINATION, \
                                                    scale=TIMESTEPS_TO_VACCINATION/3) )
                if rtime < diseaselength[s]:
                    isimmune[s]      = True
                    Nvaccinated += 1
            isinfected[s]    = False
            isinfectious[s]  = False
            issymptomatic[s] = False    
        elif isinfectious[s]:
            rtime = np.around( np.random.normal(loc=TIMESTEPS_TO_SYMPTOMS, \
                                                scale=TIMESTEPS_TO_SYMPTOMS/4) )
            if rtime < diseaselength[s]:
                issymptomatic[s] = True      
                isinfectious[s] = False
        elif isinfected[s]:
            rtime = np.around( np.random.normal(loc=TIMESTEPS_TO_INFECTIOUS, \
                                                scale=TIMESTEPS_TO_INFECTIOUS/4) )
            if rtime < diseaselength[s]:
                isinfectious[s] = True
                isinfected[s] = False

def init_for_new_run():
    global bldg_dict, Npeople, visit_pdf, plocs, Ntimesteps, Nmovespertimestep
    
    bldg_dict = load_building_data()
    Npeople,plocs = init_locations(bldg_dict)
    visit_pdf = np.array( [ .4, .4, .1, .08, .02 ] )
    reset_disease_props()
    init_bool_fields()
    init_bipartite_graph()
    Ntimesteps = 360
    Nmovespertimestep = 1000

def assemble_page():
    return sse_test_page1 + make_form() + make_chart_svg() + sse_test_page2 + make_map_svg() + sse_test_page3

@get('/')
def index():
    global bldg_dict, Npeople, visit_pdf, plocs, Ntimesteps, Nmovespertimestep
    global contact_R, contact_prob, heal_prob, die_prob, immu_prob, infe_prob
    global TIMESTEPS_TO_INFECTIOUS, TIMESTEPS_TO_SYMPTOMS, TIMESTEPS_TO_VACCINATION
    
    bldg_dict = load_building_data()
    Npeople,plocs = init_locations(bldg_dict)
    visit_pdf = np.array( [ .4, .4, .1, .08, .02 ] )
    reset_disease_props()
    init_bool_fields()
    init_bipartite_graph()
    Ntimesteps = 360

    print(immu_prob, infe_prob, contact_prob, heal_prob, die_prob, TIMESTEPS_TO_INFECTIOUS, TIMESTEPS_TO_SYMPTOMS, TIMESTEPS_TO_VACCINATION)

    return assemble_page()

@get('/svg-pan-zoom.min.js')
def return_svg_pan_zoom():
    return static_file( 'svg-pan-zoom.min.js', root='.' )

@post('/restart')
def slider_changed():
    global bldg_dict, Npeople, visit_pdf, plocs, Ntimesteps, Nmovespertimestep
    global contact_R, contact_prob, heal_prob, die_prob, immu_prob, infe_prob
    global TIMESTEPS_TO_INFECTIOUS, TIMESTEPS_TO_SYMPTOMS, TIMESTEPS_TO_VACCINATION

    contact_prob = float(request.forms.get('contact_prob'))/100
    heal_prob    = float(request.forms.get('chance_heal'))/100
    die_prob     = float(request.forms.get('chance_die'))/1000
    TIMESTEPS_TO_INFECTIOUS  = float(request.forms.get('timesteps_to_infectious'))
    TIMESTEPS_TO_SYMPTOMS    = float(request.forms.get('timesteps_to_symptomatic'))
    TIMESTEPS_TO_VACCINATION = float(request.forms.get('timesteps_to_vaccination'))
    immu_prob = float(request.forms.get('immu_prob'))/100.
    infe_prob = float(request.forms.get('infe_prob'))/1000.

    bldg_dict = load_building_data()
    Npeople,plocs = init_locations(bldg_dict)
    visit_pdf = np.array( [ .4, .4, .1, .08, .02 ] )
    ## reset_disease_props()    !! NOT HERE !!
    init_bool_fields()
    init_bipartite_graph()
    Ntimesteps = 360

    print(immu_prob, infe_prob, contact_prob, heal_prob, die_prob, TIMESTEPS_TO_INFECTIOUS, TIMESTEPS_TO_SYMPTOMS, TIMESTEPS_TO_VACCINATION)
    
    return assemble_page()

def prepare_data_string(nt):
    dstr = 'data: %d' % (nt)
    for h in [halive,himmune,hinfected,hinfectious,hsymptomatic,hdead]:
        dstr += '---'
        dstr += str(h[-1])

    qalive       = halive+himmune+hinfected+hinfectious+hsymptomatic+hdead
    qimmune      = himmune+hinfected+hinfectious+hsymptomatic+hdead
    qinfected    = hinfected+hinfectious+hsymptomatic+hdead
    qinfectious  = hinfectious+hsymptomatic+hdead
    qsymptomatic = hsymptomatic+hdead
    qdead        = hdead
    dx = 280/float(len(halive))
    dy = 100/float(np.max(qalive))
    for q in [qalive,qimmune,qinfected,qinfectious,qsymptomatic,hdead]:
        dstr += '---%f,%f ' % (0,0) #q[0]*dy)
        for iq,qq in enumerate(q):
            dstr += '%f,%f ' % (iq*dx,qq*dy)
        dstr += '%f,%f' % (len(halive)*dx,0) #q[-1]*dy)

    for field in [isalive,isimmune,isinfected,isinfectious,issymptomatic,isdead]:
        dstr += '---'
        a = bldg_dict['hpos'][plocs[field,curlocs[field]]]
        for aa in a:            
            dstr += '%f,%f ' % (aa[0]-poffsetx,-(aa[1]-poffsety))
    dstr += '\n\n'
    return dstr

@get('/stream')
def stream():
    # "Using server-sent events"
    # https://developer.mozilla.org/en-US/docs/Server-sent_events/Using_server-sent_events
    # "Stream updates with server-sent events"
    # http://www.html5rocks.com/en/tutorials/eventsource/basics/

    response.content_type  = 'text/event-stream'
    response.cache_control = 'no-cache'

    time_between_steps_ms = 1000

    # Set client-side auto-reconnect timeout, ms.
    yield 'retry: %d\n\n' % (5*time_between_steps_ms)

    n = 1

    starttime = time.time()
    for nt in range(Ntimesteps):
        print(nt)
        sys.stdout.flush()
        timestep()
        update_stats()    
        print_stats(nt)
        dstr = prepare_data_string(nt)
        sleep(.5)
        # while time.time()-starttime < nt*time_between_steps_ms:
        #     time.sleep(time_between_steps_ms/10000.)
        yield dstr
        #plot_stats(ax2,nt)
        #plot_population(ax,nt)

    
    # Keep connection alive no more then... (s)
    # end = time.time() + 60    
    # while time.time() < end:
    #     #r = np.random.random(size=(3,))
    #     curlocs = get_new_locs( Npeople, visit_pdf )
    #     dstr = ''
    #     for i,cl in enumerate(curlocs): 
    #         a = bldg_dict['hpos'][plocs[i,cl]]
    #         dstr += '%f,%f ' % (a[0]-poffsetx,-(a[1]-poffsety))
    #     yield 'data: '+dstr+' \n\n' #'data: %f %f %f \n\n' % (r[0],r[1],r[2])
    #     n += 1
    #     sleep(.1)


if __name__ == '__main__':
    run(server=GeventServer,host='0.0.0.0')
